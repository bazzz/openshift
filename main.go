package main

import (
	"log"
	"net/http"
	"path/filepath"
	"text/template"
)

const addr = "localhost:8080" // TODO Make this configurable.

var templates *template.Template
var me = Person{
	Name:    "Bas",
	Hobbies: []string{"Gaming", "Reading", "Extreme Ironing"},
}

func init() {
	templatesPath := filepath.Join("templates", "*.gohtml")
	templates = template.Must(template.New("").ParseGlob(templatesPath))
}

func main() {
	http.HandleFunc("/", handleIndex)
	http.HandleFunc("/about/", handleAbout)
	http.ListenAndServe(addr, nil)
}

func handleIndex(w http.ResponseWriter, r *http.Request) {
	if err := templates.ExecuteTemplate(w, "index.gohtml", nil); err != nil {
		log.Println("error:", err)
	}
}

func handleAbout(w http.ResponseWriter, r *http.Request) {
	if err := templates.ExecuteTemplate(w, "about.gohtml", me); err != nil {
		log.Println("error:", err)
	}
}
